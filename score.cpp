#include "score.h"
#include <QString>
#include <QDebug>

void Score::increment(){
    sec++;
    if (sec==60){ min++; sec = 0; }
    if (sec < 10){
        setPlainText(QString::number(min) + QString(" : ") + QString("0") + QString::number(sec));
    }
    else {
    setPlainText(QString::number(min) + QString(" : ") + QString::number(sec));
    }
    //qDebug("Success");
}

Score::Score()
{
    setPlainText(QString::number(min) + QString(" : ") + QString::number(sec));
    setScale(2);
    setDefaultTextColor(Qt::black);

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(increment()));
    timer->start(500);
}


/*int Score::get_score()
{
    return value;
}*/
