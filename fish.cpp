#include "fish.h"
#include "wall.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QImageReader>
#include "flappy_fish.h"

extern flappy_fish* game;

Fish::Fish(){
    QImageReader reader("/Users/yoobin/Desktop/Final_Project/fish_image.png" );
    reader.setScaledSize(QSize(30,30));
    QImage image = reader.read();
    setPixmap(QPixmap::fromImage(image));

    QTimer* timer = new QTimer();
    connect (timer, SIGNAL(timeout()), this, SLOT(move()));

    timer->start(10);
}

void Fish::keyPressEvent(QKeyEvent *event)
{
    if (!game->gameover){
        if (event->key() == Qt::Key_Space){
            setPos(x(), y()-40);
        }
    }
}

// PUBLIC SLOTS
void Fish::move()
{
    if (!game->gameover){
        setPos(x(), y()+2);
    }

}

void Fish::create_wall()
{
    Lower_Wall* lwall = new Lower_Wall();
    Upper_Wall* uwall = new Upper_Wall();
    scene()->addItem(lwall);
    scene()->addItem(uwall);
}
