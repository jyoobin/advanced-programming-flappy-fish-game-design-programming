#ifndef FLAPPY_FISH_H
#define FLAPPY_FISH_H
#include "fish.h"
#include "wall.h"
#include "score.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>

class flappy_fish: public QGraphicsView{
public:
    flappy_fish();
    QGraphicsScene* scene;
    Fish* player;
    Score* scoreboard;

    bool gameover = false;
};

#endif // FLAPPY_FISH_H
