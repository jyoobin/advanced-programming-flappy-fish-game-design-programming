#ifndef WALL_H
#define WALL_H

#include <QObject>
#include <QGraphicsRectItem>

class Wall: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Wall() = default;

public slots:
    virtual void move(){}
};

class Lower_Wall : public Wall{
    Q_OBJECT
private:
    double height;
public:
    Lower_Wall();
    void move();
    double get_height();        // accessor to the private member 'height'
};


class Upper_Wall : public Wall{
    Q_OBJECT
private:
    double height;
public:
    void move();
    Upper_Wall();
    double get_height();        // accessor to the private member 'height'
};

#endif // WALL_H
