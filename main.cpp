#include <QApplication>
#include "flappy_fish.h"

flappy_fish* game;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    game = new flappy_fish();
    game->show();
    return a.exec();
}
