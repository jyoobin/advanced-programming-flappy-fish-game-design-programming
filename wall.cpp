#include "wall.h"
#include "flappy_fish.h"
#include <QTimer>
#include <QDebug>
#include <QString>
#include <QMessageBox>
#include <QImageReader>


extern flappy_fish* game;

void Lower_Wall::move()
{
   game->player->setFocus();
   if (!game->gameover){
       setPos(x()-15,y());
            if (game->player->collidesWithItem(this)){
                game->gameover = true;
                QMessageBox msgbox;
                delete game->scoreboard;
                msgbox.setText("Game Over! You lived for " + (QString::number(game->scoreboard->get_min())) + QString(" : ")
                                                             + QString::number(game->scoreboard->get_sec()));;
                msgbox.exec();
            }

   if (this->x() < -20) { delete this;};
    }

}

void Upper_Wall::move()
{
    if (!game->gameover){
        setPos(x()-15,y());
            if (game->player->collidesWithItem(this)){
                game->gameover = true;
                QMessageBox msgbox;
                delete game->scoreboard;
                msgbox.setText("Game Over! You lived for " + (QString::number(game->scoreboard->get_min())) + QString(" : ")
                               + QString::number(game->scoreboard->get_sec()));;
                msgbox.exec();
            }

    }

    if (this->x() < -20) { delete this;};
}


double Lower_Wall::get_height()
{
    return height;
}

Upper_Wall::Upper_Wall()
{
    // Create a random height of the wall
    height = 200 + rand()%130;

    QImageReader reader("/Users/yoobin/Desktop/Final_Project/wall.png" );
    setPos(700, 0);
    reader.setScaledSize(QSize(80,height));
    QImage image = reader.read();
    setPixmap(QPixmap::fromImage(image));

    QTimer* timer = new QTimer();
    connect (timer, SIGNAL(timeout()), this, SLOT(move()));
    timer->start(50);
}

double Upper_Wall::get_height()
{
    return height;
}

Lower_Wall::Lower_Wall()
{
    // Create a random height of the wall
    height = 200 + rand()%130;

    QImageReader reader("/Users/yoobin/Desktop/Final_Project/wall.png" );
    setPos(700, 700 - get_height());
    reader.setScaledSize(QSize(80,height));
    QImage image = reader.read();
    setPixmap(QPixmap::fromImage(image));

    // Create a QTimer and connect it to the wall
    QTimer* timer = new QTimer();
    connect (timer, SIGNAL(timeout()), this, SLOT(move()));
    timer->start(50);
}
