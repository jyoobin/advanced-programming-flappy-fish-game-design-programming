#include "flappy_fish.h"
#include <QTimer>
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QImageReader>
#include <QPainter>

flappy_fish::flappy_fish()
{
    scene = new QGraphicsScene;
    scene->setSceneRect(0,0,700,700);

    QImage underwater("/Users/yoobin/Desktop/Final_Project/background.jpg");
    QPixmap pixmap(QPixmap::fromImage(underwater));
    QPixmap pixmap1 = pixmap.scaledToHeight(700);
    QGraphicsPixmapItem* background = new QGraphicsPixmapItem(pixmap1);
    background->setPos(0,0);

    setScene(scene);

    player = new Fish();
    // Add scoreboard
    scoreboard = new Score();

    scene->addItem(background);
    scene->addItem(player);
    scene->addItem(scoreboard);

  // Focus on the player
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();

    // Set the initial position of player
    // Place the fish in the middle of leftside window
    player->setPos(30, this->height()/2);

    //Fix the size of this widget
    setFixedSize(700,700);

    // Do not want the scroll bar; (in Windows)
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QTimer* timer = new QTimer;
    QObject::connect(timer, SIGNAL(timeout()), player, SLOT(create_wall()));
    timer->start(800);

    show();
}
