#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>
#include <QTimer>
#include <QObject>

class Score: public QGraphicsTextItem
{
    Q_OBJECT
private:
    int min = 0;
    int sec = 0;

public:
    Score();
    int get_min() {return min;};
    int get_sec() {return sec;};

public slots:
    void increment();

};

#endif // SCORE_H
