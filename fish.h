#ifndef FISH_H
#define FISH_H

#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include <QObject>
#include <QGraphicsScene>

class Fish : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Fish();
    void keyPressEvent(QKeyEvent* event);

public slots:
    void move();
    void create_wall();

};

#endif // FISH_H
